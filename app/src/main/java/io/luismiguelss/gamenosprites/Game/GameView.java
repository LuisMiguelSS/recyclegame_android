package io.luismiguelss.gamenosprites.Game;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.Random;

import io.luismiguelss.gamenosprites.Game.Models.Container;
import io.luismiguelss.gamenosprites.Game.Models.Rubbish;
import io.luismiguelss.gamenosprites.R;

public class GameView extends SurfaceView {

    //
    // Attributes
    //
    // Game
    private boolean gameFinished = false;
    private int score = 0;
    private int lives = 3;
    private Random random = new Random();

    // Containers
    private ArrayList<Container> containerList;

    // Rubbish
    private ArrayList<Rubbish> rubbishList;
    private Rubbish selectedRubbish;
    private float newRubbishX;
    private float newRubbishY;

    //
    // Constructor
    //
    public GameView(Context context) {
        super(context);

        setBackgroundColor(Color.WHITE);

        // Initialize rubbish list
        rubbishList = new ArrayList<>();

        // Initialize containers
        containerList = new ArrayList<>();
        containerList.add( new Container(Container.Type.Plastic, getBitmap(Container.Type.Plastic.getResource())) );
        containerList.add( new Container(Container.Type.Organic, getBitmap(Container.Type.Organic.getResource())) );
        containerList.add( new Container(Container.Type.Paper, getBitmap(Container.Type.Paper.getResource())) );

    }

    //
    // Other methods
    //
    public Bitmap getRandomImage() {
        int random = new Random().nextInt(3);
        return getBitmap(Rubbish.Type.values()[random].getResource());
    }

    public Rubbish.Type getRandomRubbishType() {
        int random = new Random().nextInt(3);
        return Rubbish.Type.values()[random];
    }

    public Bitmap getBitmap(int resource) {
        Bitmap result = null;

        try {
            BitmapFactory.Options bmpOptions = new BitmapFactory.Options();
            bmpOptions.inMutable = true;

            result = BitmapFactory.decodeResource(getContext().getResources(), resource, bmpOptions);

        } catch(Resources.NotFoundException rnf) {
            Log.e("Resource Not Found", rnf.toString());
        }

        return result;
    }

    public void showReplayMenu() {

        setBackgroundColor(Color.rgb(132,214,42));

        final Dialog dialog = new Dialog(getContext());
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.playagain_dialog);
        dialog.getWindow().setLayout((int) (getWidth()*0.55), (int)(getHeight()*0.5));//70
        dialog.setTitle("Volver a Jugar");

        ImageButton dialogButton = dialog.findViewById(R.id.btnReplay);
        dialogButton.setOnClickListener(v ->  {
            setBackgroundColor(Color.rgb(255,255,255));
            dialog.dismiss();
            gameFinished = false;
            lives = 3;
            invalidate();
        });

        // Animation In
        float deg = dialogButton.getRotation() + 360F;
        dialogButton.animate().rotation(deg).setInterpolator(new AccelerateDecelerateInterpolator());

        // Infinite rotation
        Animation rotation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate);
        rotation.setFillAfter(true);
        dialogButton.startAnimation(rotation);

        dialog.show();
    }

    //
    // Listeners
    //
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(!gameFinished) {
            // Show score
            Paint scorePaint = new Paint();
            scorePaint.setStyle(Paint.Style.FILL);
            scorePaint.setColor(Color.BLACK);
            scorePaint.setTextSize(60);
            scorePaint.setAntiAlias(true);

            canvas.drawText("Puntos: " + score, 50, 50,scorePaint);

            // Draw lives
            int heartX = getWidth()/2 - getBitmap(R.drawable.heart).getWidth()*lives;
            int heartY = 10;
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            for (int i = 0; i < lives; i++) {
                canvas.drawBitmap(getBitmap(R.drawable.heart),heartX, heartY, paint);
                heartX += getBitmap(R.drawable.heart).getWidth() + 5;
            }

            // Draw containers
            int newHeightForContainer = 125;
            for(int i = 0; i < containerList.size(); i++) {

                // New container
                Container container = containerList.get(i);

                // Get new X and Y
                int x = getWidth() - container.getImage().getWidth() - 200;
                int y = newHeightForContainer + container.getImage().getHeight()*i;

                // Draw container
                canvas.drawBitmap(container.getImage(), x, y, container);

                // Update container positions
                containerList.get(i).setX(x);
                containerList.get(i).setY(y);

                newHeightForContainer += 125;
            }

            // Add rubbish
            while (rubbishList.size() < 5) {

                Rubbish.Type rubbishType = getRandomRubbishType();

                // Generate positions
                int boundX = getWidth() -
                        getRandomImage().getWidth() -
                        (getBitmap(rubbishType.getResource()).getWidth() +
                                200) +
                        1;
                int boundY = getHeight() -
                        getRandomImage().getWidth() +
                        1;

                int randomX = random.nextInt(boundX);
                int randomY = random.nextInt(boundY) + heartY;


                Rubbish rubbish = new Rubbish(rubbishType, getBitmap(rubbishType.getResource()), randomX, randomY);

                boolean overlaps = false;
                for(Rubbish rub : rubbishList)
                    if(rub.contains(rubbish))
                        overlaps = true;

                if (!overlaps)
                    rubbishList.add(rubbish);
            }

            // Draw rubbish with new position
            for(Rubbish r: rubbishList) {
                if (r == selectedRubbish) {
                    r.setX((int)newRubbishX - r.getImage().getWidth()/2);
                    r.setY((int)newRubbishY - r.getImage().getHeight()/2);
                }
                canvas.drawBitmap(r.getImage(), r.getX(), r.getY(), r);
            }
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float eventX = event.getX();
        float eventY = event.getY();

        // Get Event
        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:
                for(Rubbish r:rubbishList) {
                    if(r.contains((int) eventX, (int) eventY)) {
                        selectedRubbish = r;
                        break;
                    }
                }
                break;

            case MotionEvent.ACTION_MOVE:
                if (selectedRubbish != null){
                    newRubbishX = eventX;
                    newRubbishY = eventY;
                    invalidate();
                }
                break;

            case MotionEvent.ACTION_UP:

                if (selectedRubbish != null) {

                    // Check for every container
                    for(Container container : containerList) {

                        if (container.contains(selectedRubbish)) {
                            if (container.isSameTipeAs(selectedRubbish)) {
                                MediaPlayer mp = MediaPlayer.create(getContext(), R.raw.matched);
                                mp.start();

                                if( ++score%10 == 0 && lives < 7)
                                    lives++;

                            } else {

                                MediaPlayer mp = MediaPlayer.create(getContext(), R.raw.error);
                                mp.start();

                                if (--lives > 0) {
                                    Toast.makeText(getContext(), "¡Te quedan " + lives + " vidas!", Toast.LENGTH_SHORT).show();

                                } else {
                                    // End game
                                    MediaPlayer mp2 = MediaPlayer.create(getContext(), R.raw.success);
                                    mp2.start();
                                    gameFinished = true;
                                    showReplayMenu();
                                }
                            }

                            rubbishList.remove(selectedRubbish);
                            invalidate();
                            break;
                        }

                    }

                    selectedRubbish = null;
                }

                break;
        }

        return true;
    }
}
