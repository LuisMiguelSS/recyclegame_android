package io.luismiguelss.gamenosprites.Game.Models;

import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;


public class Figure extends Paint {
    //
    // Attributes
    //
    private Bitmap image;
    private Rect rectangle;
    private int x;
    private int y;

    //
    // Constructor
    //
    public Figure(Bitmap image, int x, int y) {
        super();
        setAntiAlias(true);

        this.image = image;
        this.x = x;
        this.y = y;
        this.rectangle = new Rect(x, y,x+image.getWidth(),y+image.getWidth());
    }

    //
    // Getters
    //
    public Bitmap getImage() { return image; }
    public int getX() { return x; }
    public int getY() { return y; }
    public Rect getRectangle() { return rectangle; }

    //
    // Setters
    //
    public void setImage(Bitmap image) {
        this.image = image;

        rectangle.right = x + image.getWidth();
        rectangle.bottom = y + image.getHeight();
    }
    public void setX(int x) {
        this.x = x;

        rectangle.left = x;
        rectangle.right = x + image.getWidth();
    }
    public void setY(int y) {
        this.y = y;

        rectangle.top = y;
        rectangle.bottom = y + image.getHeight();
    }

    //
    // Other methods
    //
    public boolean contains(int x, int y) {
        return rectangle.contains(x,y);
    }
    public boolean contains(Figure rubbish) {
        return getRectangle().intersect(rubbish.getRectangle());
    }
}
