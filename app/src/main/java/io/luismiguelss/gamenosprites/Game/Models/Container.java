package io.luismiguelss.gamenosprites.Game.Models;

import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;

import io.luismiguelss.gamenosprites.Game.Interfaces.IFigureType;
import io.luismiguelss.gamenosprites.R;

public class Container  extends Figure implements IFigureType {

    //
    // Attributes
    //
    private Type containerType;
    public enum Type {
        Paper(R.drawable.trash_paper),
        Organic(R.drawable.trash_organic),
        Plastic (R.drawable.trash_plastic);

        int resource;
        Type(int resource) {
            this.resource = resource;
        }

        public int getResource() {return resource;}
    }

    //
    // Constructor
    //
    public Container(Type containerType, Bitmap image) {
        this(containerType, image, 0, 0);
    }
    public Container(Type containerType, Bitmap image, int x, int y) {
        super(image, x, y);
        this.containerType = containerType;
    }

    //
    // Getter
    //
    public Type getContainerType() { return containerType; }

    //
    // Other methods
    //
    public boolean isSameTipeAs(Rubbish rubbish) {
        return rubbish.getRubbishType().name().equalsIgnoreCase(this.containerType.name());
    }
}
