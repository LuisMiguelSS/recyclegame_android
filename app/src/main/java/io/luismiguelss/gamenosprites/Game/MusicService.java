package io.luismiguelss.gamenosprites.Game;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;


import io.luismiguelss.gamenosprites.R;

public class MusicService extends Service {

    //
    // Attributes
    //
    MediaPlayer player;

    //
    // Listeners
    //
    public IBinder onBind(Intent arg0) {
        return null;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        player = MediaPlayer.create(this, R.raw.background);
        player.setLooping(true);

    }
    public int onStartCommand(Intent intent, int flags, int startId) {
        player.start();
        return Service.START_STICKY;
    }

    public void onStop() {
        player.stop();
        player.release();
    }
    public void onPause() {
        player.stop();
    }

    @Override
    public void onDestroy() {
        player.stop();
        player.release();
    }

    @Override
    public void onLowMemory() {
        player.stop();
        player.release();
    }
}
