package io.luismiguelss.gamenosprites.Game.Models;

import android.graphics.Bitmap;

import io.luismiguelss.gamenosprites.Game.Interfaces.IFigureType;
import io.luismiguelss.gamenosprites.R;

public class Rubbish extends Figure implements IFigureType {

    //
    // Attribute
    //
    private Type rubbishType;

    public enum Type {
        Paper(R.drawable.rubbish_paper),
        Organic(R.drawable.rubbish_organic),
        Plastic (R.drawable.rubbish_plastic);

        int resource;
        Type(int resource) {
            this.resource = resource;
        }

        public int getResource() {return resource;}
    }

    //
    // Constructor
    //
    public Rubbish(Type rubbishType, Bitmap image, int x, int y) {
        super(image,x,y);
        this.rubbishType = rubbishType;
    }

    //
    // Getter
    //
    public Type getRubbishType() { return rubbishType; }
}
